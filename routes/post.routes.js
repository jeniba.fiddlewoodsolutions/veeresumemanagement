var express = require('express')
var userCtrl = require('../controllers/user.controller')
var authCtrl = require('../controllers/auth.controller')
var postCtrl = require('../controllers/post.controller')


var Post = require('../models/post.model')

const router = express.Router()
const multer = require("multer");
var AWS = require("aws-sdk");

var storage = multer.memoryStorage({
  destination: function (req, file, callback) {
    callback(null, '');
  }
});
var multipleUpload = multer({ storage: storage }).array('file');

router.post("/upload", multipleUpload, function (req, res) {

  const file = req.files;
  const s3FileURL = "https://veeresume.s3-us-west-2.amazonaws.com/";

  let s3bucket = new AWS.S3({
    accessKeyId: "AKIATEBD57HAOPLKOZHB",
    secretAccessKey: "XUTwHnPNhVIVQEL6vyReTURWbPEjQYX3c5wWVmnH",
    region: "us-west-2"
  });


  var ResponseData = [];
  var photo = [];
  //Where you want to store your file
  file.map((item) => {

    var params = {
      Bucket: "veeresume/FileUpload",
      Key: item.originalname,
      Body: item.buffer,
      ContentType: item.mimetype,
      ACL: "public-read"
    };
    var post = new Post();
    post.text = req.body.text;
    s3bucket.upload(params, function (err, data) {
      if (err) {
        res.status(500).json({ error: true, Message: err });
      } else {
        ResponseData.push(data);
        if (ResponseData.length == file.length) { 
          res.json({ "error": false, "Message": "File Uploaded    SuceesFully", Data: ResponseData });
        }

      }
    });
  });
});

router.route('/api/posts/new/:userId')
  .post(authCtrl.requireSignin, postCtrl.create)

router.route('/api/posts/photo/:postId')
  .get(postCtrl.photo)

router.route('/api/posts/by/:userId')
  .get(authCtrl.requireSignin, postCtrl.listByUser)

router.route('/api/posts/feed/:userId')
  .get(authCtrl.requireSignin, postCtrl.listNewsFeed)

router.route('/api/posts/like')
  .put(authCtrl.requireSignin, postCtrl.like)
router.route('/api/posts/unlike')
  .put(authCtrl.requireSignin, postCtrl.unlike)

router.route('/api/posts/comment')
  .put(authCtrl.requireSignin, postCtrl.comment)
router.route('/api/posts/uncomment')
  .put(authCtrl.requireSignin, postCtrl.uncomment)

router.route('/api/posts/:postId')
  .delete(authCtrl.requireSignin, postCtrl.isPoster, postCtrl.remove)

router.param('userId', userCtrl.userByID)
router.param('postId', postCtrl.postByID)

module.exports = router
